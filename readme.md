The Monster
===========

A Python made text adventure


To compile
===========

Run the "compile.bat" and the .exe will be generated in '/dist/'.
Alternatively, you can run the 'run.bat' and it will simply run the Python script for debugging.