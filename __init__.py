import json
import os
import sys
json_file = open("story.json")
data = json.load(json_file)

usr_input = 0 # define usr_input (probably doesn't need to be defined)

currentLoc = ""
awakenedRoomate = 0



def intro():
    os.system('COLOR 0A') #set terminal color to green
    print("|================================|")
    print("|   Welcome to \"The Monster!\"    |")
    print("|                                |")
    print("|  Copyright 2015 Ty and Bailey  |")
    print("|================================|\n")
    usr_input = raw_input("Type \"1\" to play, type \"q\" to exit.\n")
    usr_input = usr_input.lower()
    if usr_input == "q":
        sys.exit()
    elif usr_input == "1":
        os.system('COLOR 07') #set terminal color white
        os.system('cls') #clear terminal
        printpartitiontext(data["begin"]) #split the "begin" text up, so you press enter after every period
        continuefunc() #have user press enter
        main() # goto main
    else:
        #print("\nYou entered \"" + usr_input + "\"") #for debugging
        print("Invalid option, please type 1 or \"exit\"") # invalid user input handling
        raw_input()
        os.system('cls') #clear screen
        intro() #redraw intro screen
def main():
    global awakenedRoomate
    global currentLoc
    if awakenedRoomate == 0:
        decision("roomateroom", "godownstairs") #initial decision, after player wakes up
    if awakenedRoomate == 1:
        onedecision("godownstairs")
    if currentLoc == "roomateroom":
        decision("wakeroomate", "yourroom")
        if currentLoc == "wakeroomate":
            awakenedRoomate = 1
            decision("sorry", "helpme")
            if currentLoc == "sorry":
                main()
            else:
                main()
        else:
            main()
    elif currentLoc == "godownstairs":
        decision("enterbasement", "yourroom")
        if currentLoc == "enterbasement":
            onedecision("extraroomend")
        else:
            main()
    else:
        main() # if currentLoc = anything else (like beginning), goto main
    endgame() # end game

def endgame():
    os.system('cls')
    os.system('color 0a') # set color green
    print("|========================|")
    print("|        THE END         |")
    print("|------------------------|")
    print("| Thank you for playing! |")
    print("|========================|")
    raw_input("Press enter to exit")
    json_file.close() #close json
    sys.exit()
    
def continuefunc():
    raw_input("\nPress enter to continue")
    os.system('cls')
    
def decisionmade(plcm):
    global currentLoc
    output = (data[plcm]["text"]) #put json text in var
    printpartitiontext(output) # split the text up, so user presses enter after every period
    currentLoc = plcm

def printpartitiontext(input):
    #input = input[:input.rfind(".")] #slice off last period
    partitionedText = input.split(". ")#split text before period into new string in array
    
    for text in partitionedText:
        print(text + ".")
        raw_input()
    
def decision(plc1, plc2):
    global currentLoc
    usr_in2 = raw_input("\nDecision: \n1: " + data[plc1]["prettyname"] + " or 2: " + data[plc2]["prettyname"] + "? (Type 'q' to exit)\n")
    os.system('cls') #clear screen
    if usr_in2 == "1":
        decisionmade(plc1)
    elif usr_in2 == "2":
        decisionmade(plc2)
    elif usr_in2 == "q":
        sys.exit()
    else:
        print("error please enter 1 or 2")
        decision(plc1, plc2)
    continuefunc() #wait for player to press enter

def onedecision(plc):
    global currentLoc
    usr_in3 = raw_input("Press 1 to: " + data[plc]["prettyname"] + "\n")
    os.system('cls')
    if usr_in3 == "1":
        decisionmade(plc)
    else:
        print("Error please enter 1")
        onedecision(plc)
intro()
