from distutils.core import setup
import py2exe

versionVar = "1.2"
programName = "TheMonster"

setup(
    console = [{
        "script": "__init__.py", 
        "icon_resources": [(0, "icon.ico")],
        "dest_base": programName + "_" + versionVar,
    }],
    data_files = [('', ['story.json'])],
    name = programName,
    description = "A horror game with a twist.",
    version = versionVar
)